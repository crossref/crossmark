(defproject crossmark "2.3.1"
  :description "Crossmark Dialog Server"
  :url "https://crossmark.crossref.org"
  :license {:name "MIT License"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [clj-http "2.1.0"]
                 [ring "1.9.6"]
                 [org.clojure/data.json "0.2.5"]
                 [org.clojure/tools.logging "0.3.0"]
                 [http-kit "2.5.3"]
                 [javax.servlet/servlet-api "2.5"]
                 [compojure "1.1.8"]
                 [liberator "0.15.3"]
                 [robert/bruce "0.7.1"]
                 [selmer "1.0.4"]
                 [yogthos/config "0.8"]
                 [org.clojure/core.async "1.5.648"]
                 [clj-time "0.11.0"]
                 [crossref-util "0.1.16"]
                 [com.google.javascript/closure-compiler "v20131014"]
                 [org.clojure/core.cache "0.6.4"]
                 [org.clojure/core.memoize "0.5.6"]
                 [org.clojure/data.json "0.2.4"]
                 [org.clojure/tools.logging "0.3.1"]
                 [org.apache.logging.log4j/log4j-core "2.6.2"]
                 [org.slf4j/slf4j-simple "1.7.21"]
                 [com.auth0/java-jwt "2.2.1"]
                 [http-kit.fake "0.2.1"]
                 [io.sentry/sentry-clj "6.9.189"]]
  :main ^:skip-aot crossmark.core
  :target-path "target/%s"
  :plugins [[jonase/eastwood "0.2.3"]
            [lein-cloverage "1.0.9"]
            [lein-ring "0.12.6"]]
  :test-selectors {:default (constantly true)
                   :server :server
                   :browser :browser
                   :all (constantly true)}
  :jvm-opts ["-Duser.timezone=UTC" "-Dcom.sun.management.jmxremote.port=8099" "-Dcom.sun.management.jmxremote.authenticate=false" "-Dcom.sun.management.jmxremote.ssl=false"]
  :profiles {:uberjar {:aot :all}})
