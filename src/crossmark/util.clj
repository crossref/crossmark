(ns crossmark.util
  (:require [org.httpkit.client :as client]
            [crossref.util.date :as cr-date]
            [clojure.tools.logging :as log]
            [crossref.util.doi :as cr-doi]
            [clojure.data.json :as json]
            [config.core :refer [env]]
            [robert.bruce :refer [try-try-again]]
            [sentry-clj.core :as sentry])
  (:import [java.net URLEncoder]))

(defn maybe-date
  "Construct a Crossref date from a variable length vector or a nil."
  [date-vec]
  (when date-vec (apply cr-date/crossref-date date-vec)))

(def user-agent 
  "Crossmark Dialog/1.0 (https://crossmark.crossref.org; mailto:crossmark@crossref.org)")

(def headers
    (if-let [tok (:crossref-internal-api-token env)]
        {"user-agent" user-agent
         "crossref-internal-api-token" tok}
        {"user-agent" user-agent}))

(defn fetch-json
  [url opts]
  (try
    (try-try-again
      {:sleep 1000 :tries 2}
      #(let [pre-time (System/currentTimeMillis)
            opts (merge opts {:as :text
                              :socket-timeout 2000
                              :conn-timeout 2000
                              :throw-exceptions true
                              :keepalive -1
                              :headers headers})
              result @(client/get url opts)
              parsed (when (and (-> result :status #{200})
                                (-> result :headers :content-type #{"application/json"}))
                        (json/read-str (:body result) :key-fn keyword))
              post-time (System/currentTimeMillis)
              elapsed (- post-time pre-time)]

          (log/info
            (json/write-str
              {:action "api-fetch" 
               :url url 
               :pool (-> result :headers :x-api-pool) 
               :duration elapsed 
               :status (:status result) 
               :error (str (:error result))}))
          
          ; Log this now because we have headers.
          (when-let [error (:error result)]
            (sentry/send-event
              {:message (str "API fetch failed:" url ". Response headers: " (:headers result))
               :throwable error})
            (log/error (json/write-str {:action "api-fetch" :url url :error (.getMessage error)}))
            (throw error))
          parsed))
    (catch Exception e
      (do
        (sentry/send-event
          {:message (str "API fetch failed last attempt:" url)
           :throwable e})))))
          
(def works-api-endpoint
  (str (env :api-base "https://api.crossref.org") "/v1/works"))

(defn fetch-md-api [doi]
  (get-in
    (fetch-json (str works-api-endpoint "/"
                     (URLEncoder/encode doi "UTF-8")) nil)
    [:message]))

(defn fetch-reverse-updates [doi]
  (let [; Normalize because we get DOIs in all formats. We shouldn't, but sometimes the script is mucked about with.
        doi (if doi (cr-doi/non-url-doi doi) "")
        ; Only show first 100 updates. 
        updating-dois (get-in
                        (fetch-json works-api-endpoint {:query-params {"filter" (str "updates:" doi)}})
                        [:message :items])
        updates (mapcat (fn [item]
                          (map
                            (fn [update]
                              {:from (or (:DOI item) "")
                               :to (or (:DOI update) "")
                               :date (-> update :updated :date-parts first)
                               :type (:type update)
                               :label (:label update)})
                            (-> item :update-to))) updating-dois)

        relevant-updates (filter #(.equalsIgnoreCase (cr-doi/non-url-doi (:to %)) doi) updates)]
    relevant-updates))

(defn get-doi-title
  "Fetch the title for a DOI or return default.
  Makes a network request, only for use when the data isn't otherwise available."
  [doi default-value]
  (or
    (try
      (-> doi fetch-md-api :title first)
      ; Any exception (JSON or network) should return nil.
      ; fetch-md-api will have logged it.
      (catch Exception e nil))
    ; Default value on exception or if there was no title.
    default-value))

(defn possibly-normalise-doi
  "If the thing could be a DOI, normalize it. Otherwise return unchanged."
  [thing]
  (cond
    (and (string? thing)
         (cr-doi/well-formed thing))
    (cr-doi/normalise-doi thing)
    :default thing))
