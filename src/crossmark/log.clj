(ns crossmark.log
  (:require [clojure.tools.logging :refer [info warn error]]
            [clojure.java.io :as io]
            [config.core :refer [env]]
            [clojure.core.async :refer [go]])
  (:require [clj-http.client :as client])
  (:require [clojure.data.json :as json])
  (:require [robert.bruce :refer [try-try-again]])
  (:gen-class))

(defn choose-type [referrer had-updates?]
  (if had-updates?
    (condp = referrer
      :pdf "From PDF with updates"
      :unknown "Referrer not known with updates"
      nil "Referrer not known with updates"
      "From HTML with updates")
    (condp = referrer
      :pdf "From PDF"
      :unknown "Referrer not known"
      nil "Referrer not known"
      "From HTML")))

(defn log-view [doi referrer had-updates? out-of-date?]
    (let
      [type-name (choose-type referrer had-updates?)]
      (info
        (json/write-str
          {:action "view"
           :doi doi
           :referrer referrer
           :had-updates (boolean? had-updates?)
           :type type-name
           :out-of-date out-of-date?}))))
