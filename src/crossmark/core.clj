(ns crossmark.core
  (:require [config.core :refer [env]]
            [crossmark.handlers :as handlers]
            [org.httpkit.server :as server]
            [clojure.tools.logging :as log]
            [sentry-clj.core :as sentry])
(:gen-class))

(defonce s (atom nil))

(defn -main
  [& args]
  (log/info "Start server on" (:port env))
  (when-let [dsn (:sentry-dsn env)]  
    (log/info "Sentry DSN" dsn)
    (sentry/init! dsn))
  (let [port (Integer/parseInt (:port env))
        threads (Integer/parseInt (get env :threads "10"))]
  (log/info "Starting server on port " port "with" threads "threads")
  (server/run-server
    handlers/app
    {:port port
     :thread threads})))
